#ifndef ESFERA_H
#define ESFERA_H

#include "ponto.h"
#include "material.h"
#include "intersecao.h"

/*-------------------------------------------
  Objeto esfera definido matematicamente.
 *------------------------------------------*/

class Esfera
{
public:

    float raioEsfera;
    Ponto centro;
    Material material;

    Esfera( const Ponto& centro, float raioEsfera);
    Vetor getNormal(Ponto p);
    Intersecao intersecao(Ray raioEsfera);

};

#endif // ESFERA_H
