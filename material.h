#ifndef MATERIAL_H
#define MATERIAL_H

#include "cor.h"

/*---------------------------------------------------
    Componente que define características do material
    dos objetos que irão compor o cenário
 *--------------------------------------------------*/
class Material
{
public:

    // Propriedades dos materiais
    Cor kambiente;
    Cor kdifuso;
    Cor kespecular;
    float pka, pkd, pke; // Proporções
    int m; // Coeficiente de iluminação especular

    Material(const Cor& kamb, const Cor& kdif, const Cor& kespec,
             int m, float pka, float pkd, float pke);
};

#endif // MATERIAL_H
