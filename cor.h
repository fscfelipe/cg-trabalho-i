#ifndef COR_H
#define COR_H


/*----------------------------------------------------
  Componente que repersenta e guarda os valores de cor
  dos elementos do cenário.
 *---------------------------------------------------*/
class Cor
{
public:
    float r, g, b;

    Cor(float red, float green,float blue);
    Cor operator*(const Cor& cor) const;

    void setCor(float red, float green, float blue);
};

void normalizarCor(Cor &cor);

#endif // COR_H
