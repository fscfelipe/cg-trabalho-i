#include "cor.h"

Cor::Cor(float red, float green,float blue)
{
    this->r = red;
    this->g = green;
    this->b = blue;
}

Cor Cor::operator*(const Cor& cor) const
{
    return Cor((this->r*cor.r), (this->g*cor.g), (this->b*cor.b));
}

void Cor::setCor(float red, float green, float blue)
{
    this->r = red;
    this->g = green;
    this->b = blue;
}
/*--------------------------------------------------------
 Após calcular as contribuições de cores, as cores dos pontos
 calculados estarão entre (0,1). Como para pintar um pixel
 precisamos de valores entre (0,255) precisamo normalizar
 alguns casos.
 *-------------------------------------------------------*/

void normalizarCor(Cor &cor)
{
    if( cor.r > 1.0 ) cor.r = 1.0;
    if (cor.r < 0.0) cor.r = 0.0;

    if( cor.g > 1.0 )cor.g = 1.0;
    if (cor.g < 0.0) cor.g = 0.0;

    if( cor.b > 1.0 )cor.b = 1.0;
    if (cor.b < 0.0) cor.b = 0.0;
}
