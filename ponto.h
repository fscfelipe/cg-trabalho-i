#ifndef PONTO_H
#define PONTO_H

#include "vetor.h"

/*-----------------------------------------------------
 Guarda as coordenadas de um vértice em um cenário 3D
 *-----------------------------------------------------*/
class Ponto
{

public:
    float x, y, z;
    Ponto(float coordX, float coordY, float coordZ);

    /*---------------------------------------------------------
        Operações que envolvem ponto e vetor necessários para
        cálculos envolvendo câmera, objetos, iluminação, etc
     *--------------------------------------------------------*/
    Ponto operator+( const Vetor& v) const;
    Ponto operator+( const Ponto& v) const;
    Ponto operator-( const Vetor& v) const;
    Ponto operator/(float div);
    Vetor operator-( const Ponto& p) const;
    Vetor paraVetor();

};

#endif // PONTO_H
