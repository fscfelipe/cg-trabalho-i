#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /*----------------------------------------------------------------------*
      Métodos executados quando a janela for 'carregada'
     *----------------------------------------------------------------------*/
    definirCenario();
    renderizarCena();
}
/*---------------------------------------------------------------------------*
  Método utilizado para definir o nosso cenário, adicionando objetos e
  fonts de iluminação no mesmo.
 *--------------------------------------------------------------------------*/
void MainWindow::definirCenario(){
    adicionarSnowMan();
}

/*---------------------------------------------------------------------------*
  Método que irá interagir e renderizar todos os objetos que pertencem ao
  cenário.
 *---------------------------------------------------------------------------*/
void MainWindow::renderizarCena(){

    /*------------------------------------------------------
      Pegamos os tamanhos da largura e altura do GraphicView
      e criamos uma imagem que irá receber os pixels a serem
      definidos...
    *------------------------------------------------------*/
    QSize janela =  ui->graphicsView->size();
    int alturaJanela = janela.height();
    int larguraJanela = janela.width();

    QImage imagem = QImage( larguraJanela, alturaJanela, QImage::Format_RGB32 );

    /*-------------------------------------------------------
      Iremos percorrer todos os pixels da imagem para atribuir
      um valor de cor para cada pixel.
      # de cima para baixo - da esquerda para direita)
    *-------------------------------------------------------*/
    for( int linha = 0 ; linha < alturaJanela; linha++){ // y
        for( int coluna = 0 ; coluna < larguraJanela; coluna++){ // x

            // cria um raio para cada posição do plano de projeção
            Ray ray = camera->lancarRaio(coluna,linha);

            Intersecao pInt;

            // Verifica para todos os objetos do cenário, se o raio intercepta
            // algum ponto para ser pintado.
            if(cenario.intersecao(ray, pInt))
            {
                /*-------------------------------------------------
                  Aqui temos a certeza de um ponto interceptado
                  agora iremos calcular valores de iluminação, etc.
                 *------------------------------------------------*/
                Cor corPonto(0,0,0);
                corPonto = calcularCorAmbiente(pInt.material);

                /*---------------------------------------------------
                  Aqui iremos percorrer a lista de fontes de iluminação
                  do cenário para calcular as contribuições de cor,
                  sombra, etc.
                 *--------------------------------------------------*/
                for(int indiceLuz=0; indiceLuz < cenario.fontesIluminacao.size(); indiceLuz++)
                {
                    Cor corIluminacao = cenario.fontesIluminacao[indiceLuz].cor;

                    // Vetor L, V, R
                    Vetor L = cenario.fontesIluminacao[indiceLuz].posicao - pInt.ponto;
                    L.normalizar();
                    Vetor R = calcularRaioRefletido(L,pInt.normal);
                    Vetor V = (ray.origem.paraVetor() - ray.direcao);
                    V.normalizar();
                    float RV = produtoEscalar( V, R );

                    float atenuacaoDifusa = produtoEscalar(pInt.normal, L);

                    // Não há contribuição quando for negativo
                    if( atenuacaoDifusa < 0.0 )
                        atenuacaoDifusa = 0.0;
                    if( RV < 0.0  )
                        RV = 0.0;

                    //Cor difusa
                    corPonto = calcularCorDifusa(corPonto, corIluminacao ,pInt.material, atenuacaoDifusa);

                    //Cor Especular
                    corPonto = calcularCorEspecular(corPonto, corIluminacao ,pInt.material, RV);

                }

                // Comor a cor está entre [0,1], precisamos corrigir quem está fora disso
                normalizarCor(corPonto);

                // pinta o pixel com a cor definida
                imagem.setPixel( coluna, linha, qRgb( corPonto.r*255, corPonto.g*255, corPonto.b*255) );

            }
            else
            {
                //Caso não haja intersecao, pinta cor de background
                imagem.setPixel( coluna, linha, qRgb(10, 10, 10) );
            }

        }
    }

    /*--------------------------------------------------------
     Precisamos definir um GraphicsScene onde iremos mapear a
     imagem, e depois adicionar essa cena no GraphicsView.
    *--------------------------------------------------------*/
    QPixmap mapaPixel = QPixmap::fromImage(imagem);
    QGraphicsScene *cenarioMapeado = new QGraphicsScene(this);
    cenarioMapeado->addPixmap(mapaPixel);
    ui->graphicsView->setScene(cenarioMapeado);

}

void MainWindow::adicionarSnowMan(){

    QSize janela =  ui->graphicsView->size();
    int alturaJanela = janela.height();
    int larguraJanela = janela.width();

    // Definição dos materiais
    Material materialVermelho( Cor(0.2, 0.0, 0.0), Cor(1.0, 0, 0), Cor(0.2, 0.2, 0.2), 200, 1.0, 1.0, 0.5 );
    Material materialBranco( Cor(0.2, 0.2, 0.2), Cor(1.0, 1.0, 1.0), Cor(1.0, 1.0, 1.0), 200, 1.0, 1.0, 0.05 );
    Material materialPreto( Cor(0.1, 0.1, 0.1), Cor(0.1, 0.1, 0.1), Cor(0.1, 0.1, 0.1), 200, 1.0, 1.0, 0.05 );

    // Corpo do snowman
    cenario.adicionaEsfera( Ponto(0,0,0), 4, materialBranco );

    // Cabeça do snowman
    cenario.adicionaEsfera( Ponto(0,5.5,0), 2, materialBranco );

    // Botões do snowman
    cenario.adicionaEsfera( Ponto(0,2,3.5), 0.5, materialVermelho );
    cenario.adicionaEsfera( Ponto(0,0,4), 0.5, materialVermelho );
    cenario.adicionaEsfera( Ponto(0,-2,3.5), 0.5, materialVermelho );

    // Olhos do snowman
    cenario.adicionaEsfera( Ponto(-0.8,6,1.7), 0.3, materialPreto );
    cenario.adicionaEsfera( Ponto(0.8,6,1.7), 0.3, materialPreto );

    // Luz
    cenario.adicionaIluminacao(Ponto(0, 10, 20), 1.0, 1.0, 1.0, 0.0001);


    // Câmera
    camera = new Camera(larguraJanela, alturaJanela);
    camera->setPos( Ponto(10, 10, 10) );
    camera->lookAt( Ponto(0, 0, 0) );
}

MainWindow::~MainWindow()
{
    delete ui;
}
