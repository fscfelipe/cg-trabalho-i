#include "iluminacao.h"

Iluminacao::Iluminacao():
    cor(Cor(0,0,0)),posicao(0,0,0),atenuacao(0)
{
}


Cor calcularCorAmbiente(Material material){

    float r = (material.kambiente.r) * (material.pka);
    float g = (material.kambiente.g) * (material.pka);
    float b = (material.kambiente.b) * (material.pka);
    return Cor(r,g,b);

}

Cor calcularCorDifusa(Cor corPonto, Cor corFonte, Material materialPonto, float atenucao)
{
    float r = corFonte.r * materialPonto.kdifuso.r * materialPonto.pkd * atenucao;
    float g = corFonte.g * materialPonto.kdifuso.g * materialPonto.pkd * atenucao;
    float b = corFonte.b * materialPonto.kdifuso.b * materialPonto.pkd * atenucao;

    return Cor(corPonto.r+r, corPonto.g+g, corPonto.b+b);
}

Cor calcularCorEspecular(Cor corPonto, Cor corFonte, Material materialPonto, float raioRefletido)
{
    float r =  corFonte.r * materialPonto.kespecular.r* materialPonto.pke * pow(raioRefletido, materialPonto.m);
    float g =  corFonte.g * materialPonto.kespecular.g* materialPonto.pke * pow(raioRefletido, materialPonto.m);
    float b =  corFonte.b * materialPonto.kespecular.b* materialPonto.pke * pow(raioRefletido, materialPonto.m);

    return Cor(corPonto.r+r, corPonto.g+g, corPonto.b+b);
}

Vetor calcularRaioRefletido(Vetor L, Vetor N)
{
    Vetor R = L - ( N * (2.0 * produtoEscalar(L,N) ) );
    R.normalizar();
    return R;
}
