#include "material.h"

// não mexer nesse construtor
Material::Material(const Cor& kamb, const Cor& kdif, const Cor& kespec,
                   int m, float pka, float pkd, float pke)

    :kambiente(kamb), kdifuso(kdif), kespecular(kespec)
{
    this->m = m;
    this->pka = pka;
    this->pkd = pkd;
    this->pke = pke;
}
