#ifndef ILUMINACAO_H
#define ILUMINACAO_H

#include "cor.h"
#include "ponto.h"
#include "material.h"

/*------------------------------------------------
 Define propriedades de fontes de iluminação
 que serão adicionados no cenário
  *---------------------------------------------*/

// OBS.: precisamos construir os tipos de iluminação...

class Iluminacao
{
public:
    Cor cor;
    Ponto posicao;
    float atenuacao;

    Iluminacao();
};

Vetor calcularRaioRefletido(Vetor L, Vetor N);
Cor calcularCorAmbiente(Material material);
Cor calcularCorDifusa(Cor corPonto, Cor corFonte, Material materialPonto, float atenucao);
Cor calcularCorEspecular(Cor corPonto, Cor corFonte, Material materialPonto, float raioRefletido);


#endif // ILUMINACAO_H
