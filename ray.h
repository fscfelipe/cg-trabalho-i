#ifndef RAY_H
#define RAY_H

#include "ponto.h"
#include "vetor.h"


/*-----------------------------------------------------------
  A definição de uma classe raio é necessária, pois a partir
  da câmera iremos criar um raio, onde iremos passar por
  parâmetro para todos os métodos de interseção de todos os
  objetos do cenário.
 *---------------------------------------------------------*/
class Ray
{
public:

    Ponto origem;
    Vetor direcao;
    Ray(Ponto& origem, Vetor& direcao);

};

#endif // RAY_H



/*Fontes:
 1. https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays/definition-ray
*/
