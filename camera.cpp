#include "camera.h"

// Valores padrões definidos (não modificar)
Camera::Camera(): ic(Vetor(1.0, 0.0, 0.0))
              , jc(Vetor(0.0, 1.0, 0.0))
              , kc(Vetor(0.0, 0.0, 1.0))
              , posicao(0.0, 0.0, 10.0)
{
   this->lookAt( Ponto{0, 0, 0} );
}

// Valores padrões definidos (não modificar)
Camera::Camera(int imgLargura, int imgAltura)
    :   ic(Vetor(1.0, 0.0, 0.0))
      , jc(Vetor(0.0, 1.0, 0.0))
      , kc(Vetor(0.0, 0.0, 1.0))
      , posicao(0.0, 0.0, 10.0)
{
    alturaImg = imgAltura;
    larguraImg = imgLargura;

    this->setPos( Ponto{0, 0, 10.0} );
    this->lookAt( Ponto{0, 0, 0} );
}

/*------------------------------------------------------------------
  Lança raios a partir da câmera levando em consideração o plano
  definido para projeção, mapeando cada pixel em uma definição
  de um cubo 2x2.

  - Fonte: https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays/generating-camera-rays
 *-----------------------------------------------------------------*/
Ray Camera::lancarRaio(int xImagem, int yImagem)
{
    /*--------------------------------------------------------------
     Define as coordenadas do pixel em um cubo 2x2, dado que a câmera
     está em (0,0,-1).
     *--------------------------------------------------------------*/
     float fov = 90; // Field of View
     float imagemAspectRatio = larguraImg / (float) alturaImg; // assuming width > height
     float xPixel = (2 * ((xImagem + 0.5) / larguraImg) - 1) * tan(fov / 2 * M_PI / 180) * imagemAspectRatio;
     float yPixel = (1 - 2 * ((yImagem + 0.5) / alturaImg) * tan(fov / 2 * M_PI / 180));

     /*------------------------------------------------------------
       Como podemos modificar a posição da câmera, é necessário recalcular

      *----------------------------------------------------------*/
     Ponto pPixel = posicao + kc*(-1.0) + ic*xPixel + jc*yPixel;
     Vetor direcao = pPixel - posicao;
     direcao.normalizar();

     // Cria raio a partir da posição da camera em relação ao ponto projetado
     return Ray(posicao, direcao);

}

/*-----------------------------------------------------------------
  Após setar o lookAt, calculamos os novos vetores ic, jc, kc
  da câmera.
*-----------------------------------------------------------------*/
void Camera::lookAt( Ponto lookAt ){

    // kc = lookAt_Po / || lookAt_Po ||
    this->kc = this->posicao - lookAt;
    this->kc.normalizar();

    // ic = (ViewUp x kc ) / || ViewUp x kc ||
    Vetor viewUp = (lookAt + Ponto(0, 1.0, 0)) - this->posicao ;
    this->ic = produtoVetorial( viewUp, this->kc );
    this->ic.normalizar();

    // jc = kc x ic
    this->jc = produtoVetorial( this->kc, this->ic  );

}

Ponto Camera::getPos(){
    return this->posicao;
}

void Camera::setPos(Ponto posicao){
    this->posicao = posicao;
}
