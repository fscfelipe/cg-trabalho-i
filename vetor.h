#ifndef VETOR_H
#define VETOR_H

#include "cmath"

/*-----------------------------------------------------
 Guarda as coordenadas de um vetor em um cenário 3D
 *-----------------------------------------------------*/
class Vetor
{

public:
    float x,y,z;

    /*---------------------------------------------------
      Operações com vetores...
     *-------------------------------------------------*/
    Vetor(float coordX, float coordY, float coordZ);
    Vetor operator+(const Vetor& v) const;
    Vetor operator-(const Vetor& v) const;
    Vetor operator*(float f) ;
    Vetor operator/(float f);

    /*--------------------------------------------------
     Normalizar um vetor ou calcular o seu comprimento
     são operações nescessárias para os cálculos do raycast
     como por exemplo normalizar os vetores de iluminação
     *-------------------------------------------------*/
    float modulo();
    void normalizar();
};

/*-------------------------------------------------------
 Operações definidas fora da classe, pois não precisamos
 instanciar um vetor para executá-las.
*-------------------------------------------------------*/
float produtoEscalar( Vetor& v1, Vetor& v2);
Vetor produtoVetorial( Vetor& v1, Vetor& v2);
Vetor normalizarVetor( Vetor& vetor);

#endif // VETOR_H
