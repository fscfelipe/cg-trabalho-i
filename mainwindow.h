#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>

#include "cenario.h"
#include "camera.h"
#include "ponto.h"
#include "vetor.h"
#include "material.h"
#include "cor.h"
#include "iluminacao.h"

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Cenario cenario;
    Camera *camera;

    void adicionarSnowMan();
    void definirCenario();
    void renderizarCena();

};

#endif // MAINWINDOW_H
