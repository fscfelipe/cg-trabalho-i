#include "esfera.h"

Esfera::Esfera(const Ponto &center, float radius)
    : centro(center), raioEsfera(radius),
      material(Material( Cor(0,0,0), Cor(0,0,0), Cor(0,0,0),0,0,0,0 ))
{
}


Intersecao Esfera::intersecao(Ray ray)
{

    Vetor centroEsfera = Vetor(centro.x, centro.y, centro.z);
    Vetor origemRaio = Vetor(ray.origem.x, ray.origem.y, ray.origem.z);

    /*---------------------------------------------------------
      V = Direção do raio
      E = Origem do observador
      C = Centro da Esfera
     *--------------------------------------------------------*/
    float VV = produtoEscalar(ray.direcao, ray.direcao);
    float EV = produtoEscalar(origemRaio, ray.direcao);
    float VC = produtoEscalar(ray.direcao, centroEsfera);
    float CC = produtoEscalar(centroEsfera, centroEsfera);
    float EC = produtoEscalar(centroEsfera, origemRaio);
    float EE = produtoEscalar(origemRaio, origemRaio);

    float eqa = VV;
    float eqb = 2*( EV - VC );
    float eqc = CC - 2*EC + EE - raioEsfera*raioEsfera;

    float delta = eqb*eqb - 4*eqa*eqc;

    Intersecao pInt;
    pInt.distancia = 0.0;

    if (delta < 0) {
        // Sem interseção
        return pInt;
    } else if (delta == 0) {

        // uma interseção
        Intersecao pInt;
        pInt.distancia = -eqb/(2*eqa);
        pInt.ponto = ray.origem + ray.direcao*pInt.distancia;
        pInt.material = this->material;
        pInt.normal  = this->getNormal(pInt.ponto);

    } else {

        // Duas interceções, retorn mais próxima
        float sqrtDelta = sqrtf(delta);
        float t0 = (-eqb - sqrtDelta)/(2*eqa);
        float t1 = (-eqb + sqrtDelta)/(2*eqa);

        float pIntMaisProximo = (t0 < t1) ? t0 : t1;

        pInt.distancia = pIntMaisProximo;
        pInt.ponto = ray.origem + ray.direcao*pInt.distancia;
        pInt.normal  = this->getNormal(pInt.ponto);
        pInt.material = this->material;
    }

    return pInt;
}

Vetor Esfera::getNormal(Ponto ponto){
    return (ponto - centro)/raioEsfera;
}
