#include "cenario.h"
#include <iostream>
using namespace std;

Cenario::Cenario()
{

}

/*----------------------------------------------------------
 Para todo objeto no cenário verificamos se há interseção
*----------------------------------------------------------*/
bool Cenario::intersecao( Ray &ray, Intersecao &intersecao)
{
    // Variável distância utilizada para verificar o primeiro ponto
    // interceptado, que será então atualizada;
    float distancia = 9999999;
    Intersecao pIntMaisProximo;

    // Para cada esfera adicionada no cenário, verificamos se há interceção
    // e retornamos o ponto mais próximo dentre todos os pontos interceptados

    for(auto const& esfera: esferas ){
        Intersecao pInt = esfera->intersecao(ray);

        if( pInt.distancia > 0.0 && pInt.distancia <= distancia )
        {
            distancia = pInt.distancia;
            pIntMaisProximo = pInt;
        }
    }

    // Não houve interseção
    if ( distancia == 9999999 )
        return false;

    intersecao = pIntMaisProximo;

    return true;

}

void Cenario::adicionaIluminacao( const Ponto &posicao, float r, float g, float b, float atenuacao)
{
    Iluminacao luz;
    luz.cor.setCor(r, g, b);
    luz.posicao = posicao;
    luz.atenuacao = atenuacao;
    fontesIluminacao.push_back(luz);
}

void Cenario::adicionaEsfera( const Ponto& centro, float raio, Material material)
{
    Esfera *novaEsfera = new Esfera(centro, raio);
    novaEsfera->material=material;
    esferas.push_back(novaEsfera);
}
